# DOTFILES
## Everyday software:
 - WSL 2 (Ubuntu 18.04) with Windows Terminal
 - Docker on WSL
 - Jetbrains toolbox
   - Clion for C++ development
   - Phpstorm for web development
   - Pycharm for python development
   - Intellij IDEA for JAVA development
 - Visual Studio Code as simple code editor
 - 1password as a password manager
 - Adobe XD for working with designs
 - AutoHotkey
 - chrome as main browser
 - discord for communication
 - TIDAL as a music player