export ZSH="$HOME/.oh-my-zsh"

ZSH_THEME="robbyrussell"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

plugins=(git ssh-agent zsh-autosuggestions fast-syntax-highlighting)

source $ZSH/oh-my-zsh.sh

alias ll="ls -la"
alias pstorm="/mnt/c/Users/brzoz/AppData/Local/JetBrains/PhpStorm\ 2021.3.2/bin/phpstorm64.exe ."

# Loading nvm
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
